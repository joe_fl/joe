// values to keep track of the number of letters typed, which quote to use. etc. Don't change these values.
var i = 0,
    a = 0,
    isBackspacing = false,
    isParagraph = false;

// Typerwrite text content. Use a pipe to indicate the start of the second line "|".  
var textArray = [
    "Hi Plumz.....!",
    "Now I wan't to say something special to you.",
    "So , Please read everything carefully...!",
    "When I saw you for the first time | You seems something Special to me.",
    "As the days goes, you get closer to me....!",
    "I don't know the reason why your thoughts always resonates inside my mind...!",
    "Everything about you is always intresting for me...!",
    "I am somewhat nervous, Because I haven't said these words to anyone and I won't say to anyone in future...!",
    "I Love my Parents so much than anything else in this world....!",
    "Now You are the only person  whom I love equally with my parents....!",
    "I Love U <Plumz kutty.....!",
    "Give me One chance to Prove my Love ...!"
];

// Speed (in milliseconds) of typing.
var speedForward = 100, //Typing Speed
    speedWait = 1000, // Wait between typing and backspacing
    speedBetweenLines = 1000, //Wait between first and second lines
    speedBackspace = 25; //Backspace Speed

$(document).ready(function() {
    //Run the loop
    typeWriter("output", textArray);

    function typeWriter(id, ar) {
        var element = $("#" + id),
            aString = ar[a],
            eHeader = element.children("h1"), //Header element
            eParagraph = element.children("p"); //Subheader element

        // Determine if animation should be typing or backspacing
        if (!isBackspacing) {

            // If full string hasn't yet been typed out, continue typing
            if (i < aString.length) {

                // If character about to be typed is a pipe, switch to second line and continue.
                if (aString.charAt(i) == "|") {
                    isParagraph = true;
                    eHeader.removeClass("cursor");
                    eParagraph.addClass("cursor");
                    i++;
                    setTimeout(function() { typeWriter(id, ar); }, speedBetweenLines);

                    // If character isn't a pipe, continue typing.
                } else {
                    // Type header or subheader depending on whether pipe has been detected
                    if (!isParagraph) {
                        eHeader.text(eHeader.text() + aString.charAt(i));
                    } else {
                        eParagraph.text(eParagraph.text() + aString.charAt(i));
                    }
                    i++;
                    setTimeout(function() { typeWriter(id, ar); }, speedForward);
                }

                // If full string has been typed, switch to backspace mode.
            } else if (i == aString.length) {

                isBackspacing = true;
                setTimeout(function() { typeWriter(id, ar); }, speedWait);

            }

            // If backspacing is enabled
        } else {

            // If either the header or the paragraph still has text, continue backspacing
            if (eHeader.text().length > 0 || eParagraph.text().length > 0) {

                // If paragraph still has text, continue erasing, otherwise switch to the header.
                if (eParagraph.text().length > 0) {
                    eParagraph.text(eParagraph.text().substring(0, eParagraph.text().length - 1));
                } else if (eHeader.text().length > 0) {
                    eParagraph.removeClass("cursor");
                    eHeader.addClass("cursor");
                    eHeader.text(eHeader.text().substring(0, eHeader.text().length - 1));
                }
                setTimeout(function() { typeWriter(id, ar); }, speedBackspace);

                // If neither head or paragraph still has text, switch to next quote in array and start typing.
            } else {

                isBackspacing = false;
                i = 0;
                isParagraph = false;
                a = (a + 1) % ar.length; //Moves to next position in array, always looping back to 0
                setTimeout(function() { typeWriter(id, ar); }, 50);

            }
        }
    }

});


(function() {
    var TOTAL_BANDS, analyser, analyserDataArray, arrCircles, audio, build, buildCircles, canplay, changeMode, changeTheme, circlesContainer, cp, createCircleTex, gui, hammertime, init, initAudio, initGUI, initGestures, isPlaying, k, message, modes, mousePt, mouseX, mouseY, params, play, renderer, resize, stage, startAnimation, texCircle, themes, themesNames, update, v, windowH, windowW;

    modes = ["cubic", "conic"];

    themes = {
        white: [0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF],
        red: [0xFF0000, 0xFF0000, 0xFF0000, 0xFF0000, 0xFF0000],
        whiteRed: [0xFFFFFF, 0xFF0000, 0xFFFFFF, 0xFF0000, 0xFFFFFF],
        redWhite: [0xFF0000, 0xFFFFFF, 0xFF0000, 0xFFFFFF, 0xFF0000],
        holiday: [0xFF0000, 0xFFFFFF, 0x32CD32, 0xFFFFFF, 0xFF0000, 0x32CD32]
    };

    themesNames = [];

    for (k in themes) {
        v = themes[k];
        themesNames.push(k);
    }

    // PARAMETERS
    params = {
        // public
        mode: modes[0],
        theme: themesNames[0],
        radius: 1,
        distance: 1000,
        size: 1,
        // private
        numParticles: 5000,
        sizeW: 1,
        sizeH: 1,
        radiusParticle: 60,
        themeArr: themes[this.theme]
    };

    TOTAL_BANDS = 256;

    cp = new PIXI.Point();

    mouseX = 0;

    mouseY = 0;

    mousePt = new PIXI.Point();

    windowW = 0;

    windowH = 0;

    stage = null;

    renderer = null;

    texCircle = null;

    circlesContainer = null;

    arrCircles = [];

    hammertime = null;

    message = null;

    // audio
    audio = null;

    analyser = null;

    analyserDataArray = null;

    isPlaying = false;

    canplay = false;

    // gui
    gui = null;

    init = function() {
        initGestures();
        message = $(".message");
        message.on("click", play);
        resize();
        build();
        resize();
        mousePt.x = cp.x;
        mousePt.y = cp.y;
        $(window).resize(resize);
        startAnimation();
        initGUI();
        return initAudio();
    };

    play = function() {
        if (isPlaying) {
            return;
        }
        message.css("cursor", "default");
        if (canplay) {
            message.hide();
        } else {
            message.html("LOADING MUSIC...");
        }
        audio.play();
        return isPlaying = true;
    };

    initGUI = function() {
        var modeController, sizeController, themeController;
        gui = new dat.GUI();
        // if window.innerWidth < 500
        gui.close();
        modeController = gui.add(params, 'mode', modes);
        modeController.onChange(function(value) {
            return changeMode(value);
        });
        themeController = gui.add(params, 'theme', themesNames);
        themeController.onChange(function(value) {
            return changeTheme(params.theme);
        });
        gui.add(params, 'radius', 1, 8);
        gui.add(params, 'distance', 100, 1000);
        sizeController = gui.add(params, 'size', 0, 1);
        return sizeController.onChange(function(value) {
            return resize(value);
        });
    };

    initAudio = function() {
        var context, source;
        context = new(window.AudioContext || window.webkitAudioContext)();
        analyser = context.createAnalyser();
        //   analyser.smoothingTimeConstant = 0.5
        source = null;
        audio = new Audio();
        audio.crossOrigin = "anonymous";
        audio.src = "./media/audio/joe.mp3";
        return audio.addEventListener('canplay', function() {
            var bufferLength;
            if (isPlaying) {
                message.hide();
            }
            canplay = true;
            source = context.createMediaElementSource(audio);
            source.connect(analyser);
            source.connect(context.destination);
            analyser.fftSize = TOTAL_BANDS * 2;
            bufferLength = analyser.frequencyBinCount;
            return analyserDataArray = new Uint8Array(bufferLength);
        });
    };

    startAnimation = function() {
        return requestAnimFrame(update);
    };

    initGestures = function() {
        return $(window).on('mousemove touchmove', function(e) {
            if (e.type === 'mousemove') {
                mouseX = e.clientX;
                return mouseY = e.clientY;
            } else {
                mouseX = e.originalEvent.changedTouches[0].clientX;
                return mouseY = e.originalEvent.changedTouches[0].clientY;
            }
        });
    };

    build = function() {
        stage = new PIXI.Stage(0x000000);
        renderer = PIXI.autoDetectRenderer({
            width: $(window).width(),
            height: $(window).height(),
            antialias: true,
            resolution: window.devicePixelRatio
        });
        $(document.body).append(renderer.view);
        texCircle = createCircleTex();
        return buildCircles();
    };

    buildCircles = function() {
        var circle, i, j, ref;
        circlesContainer = new PIXI.DisplayObjectContainer();
        stage.addChild(circlesContainer);
        for (i = j = 0, ref = params.numParticles - 1;
            (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
            circle = new PIXI.Sprite(texCircle);
            circle.anchor.x = 0.5;
            circle.anchor.y = 0.5;
            circle.position.x = circle.xInit = cp.x;
            circle.position.y = circle.yInit = cp.y;
            circle.mouseRad = Math.random();
            circlesContainer.addChild(circle);
            arrCircles.push(circle);
        }
        return changeTheme(params.theme);
    };

    createCircleTex = function() {
        var gCircle;
        gCircle = new PIXI.Graphics();
        gCircle.beginFill(0xFFFFFF);
        gCircle.drawCircle(0, 0, params.radiusParticle);
        gCircle.endFill();
        return gCircle.generateTexture();
    };

    resize = function() {
        windowW = $(window).width();
        windowH = $(window).height();
        cp.x = windowW * .5;
        cp.y = windowH * .5;
        params.sizeW = windowH * params.size;
        params.sizeH = windowH * params.size;
        changeMode(params.mode);
        if (renderer) {
            return renderer.resize(windowW, windowH);
        }
    };

    changeTheme = function(name) {
        var circle, group, i, indexColor, j, padColor, ref, results;
        params.themeArr = themes[name];
        indexColor = 0;
        padColor = Math.ceil(params.numParticles / params.themeArr.length);
        results = [];
        for (i = j = 0, ref = params.numParticles - 1;
            (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
            circle = arrCircles[i];
            group = indexColor * padColor / params.numParticles;
            circle.blendMode = params.theme === "blackWhite" ? PIXI.blendModes.NORMAL : PIXI.blendModes.ADD;
            circle.indexBand = Math.round(group * (TOTAL_BANDS - 56)) - 1;
            if (circle.indexBand <= 0) {
                circle.indexBand = 49;
            }
            circle.s = (Math.random() + (params.themeArr.length - indexColor) * 0.2) * 0.1;
            circle.scale = new PIXI.Point(circle.s, circle.s);
            if (i % padColor === 0) {
                indexColor++;
            }
            results.push(circle.tint = params.themeArr[indexColor - 1]);
        }
        return results;
    };

    changeMode = function(value) {
        var angle, circle, i, j, ref, results;
        if (!arrCircles || arrCircles.length === 0) {
            return;
        }
        if (!value) {
            value = modes[Math.floor(Math.random() * modes.length)];
        }
        params.mode = value;
        results = [];
        for (i = j = 0, ref = params.numParticles - 1;
            (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
            circle = arrCircles[i];
            switch (params.mode) {
                // cubic
                case modes[0]:
                    circle.xInit = cp.x + (Math.random() * params.sizeW - params.sizeW / 2);
                    results.push(circle.yInit = cp.y + (Math.random() * params.sizeH - params.sizeH / 2));
                    break;
                    // circular
                case modes[1]:
                    angle = Math.random() * (Math.PI * 2);
                    circle.xInit = cp.x + (Math.cos(angle) * params.sizeW);
                    results.push(circle.yInit = cp.y + (Math.sin(angle) * params.sizeH));
                    break;
                default:
                    results.push(void 0);
            }
        }
        return results;
    };

    update = function() {
        var a, angle, circle, dist, dx, dy, i, j, n, r, ref, scale, t, xpos, ypos;
        requestAnimFrame(update);
        t = performance.now() / 60;
        if (analyserDataArray && isPlaying) {
            analyser.getByteFrequencyData(analyserDataArray);
        }
        if (mouseX > 0 && mouseY > 0) {
            mousePt.x += (mouseX - mousePt.x) * 0.03;
            mousePt.y += (mouseY - mousePt.y) * 0.03;
        } else {
            a = t * 0.05;
            mousePt.x = cp.x + Math.cos(a) * 100;
            mousePt.y = cp.y + Math.sin(a) * 100;
        }
        for (i = j = 0, ref = params.numParticles - 1;
            (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
            circle = arrCircles[i];
            if (analyserDataArray && isPlaying) {
                n = analyserDataArray[circle.indexBand];
                scale = (n / 256) * circle.s * 2;
            } else {
                scale = circle.s * .1;
            }
            scale *= params.radius;
            circle.scale.x += (scale - circle.scale.x) * 0.3;
            circle.scale.y = circle.scale.x;
            dx = mousePt.x - circle.xInit;
            dy = mousePt.y - circle.yInit;
            dist = Math.sqrt(dx * dx + dy * dy);
            angle = Math.atan2(dy, dx);
            r = circle.mouseRad * params.distance + 30;
            xpos = circle.xInit - Math.cos(angle) * r;
            ypos = circle.yInit - Math.sin(angle) * r;
            circle.position.x += (xpos - circle.position.x) * 0.1;
            circle.position.y += (ypos - circle.position.y) * 0.1;
        }
        return renderer.render(stage);
    };

    init();

}).call(this);



// audio

var audio, playbtn, mutebtn, seek_bar;

function initAudioPlayer() {
    audio = new Audio();
    audio.src = "./media/audio/joe.mp3";
    audio.loop = true;
    audio.play();
    // Set object references
    playbtn = document.getElementById("playpausebtn");
    mutebtn = document.getElementById("mutebtn");
    // Add Event Handling
    playbtn.addEventListener("click", playPause);
    mutebtn.addEventListener("click", mute);
    // Functions
    function playPause() {
        if (audio.paused) {
            audio.play();
            playbtn.style.background = "url(https://image.flaticon.com/icons/svg/747/747967.svg) no-repeat";
        } else {
            audio.pause();
            playbtn.style.background = "url(https://image.flaticon.com/icons/svg/747/747965.svg) no-repeat";
        }
    }

    function mute() {
        if (audio.muted) {
            audio.muted = false;
            mutebtn.style.background = "url(https://image.flaticon.com/icons/svg/660/660545.svg) no-repeat";
        } else {
            audio.muted = true;
            mutebtn.style.background = "url(https://image.flaticon.com/icons/svg/455/455742.svg) no-repeat";
        }
    }
}
window.addEventListener("load", initAudioPlayer);